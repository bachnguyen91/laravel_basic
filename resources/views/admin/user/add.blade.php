@extends('admin.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="col-sm-12">
                <form action="{{ route('admin.user.save') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <input name="name" required placeholder="Username">
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <input name="email" required placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <input name="password" type="password" required placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <input value="submit" type="submit">
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection